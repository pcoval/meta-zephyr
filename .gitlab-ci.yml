# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

stages:
  - compliance
  - build-gcc
  - build-clang
  - test
  - report

include:
 - project: 'eclipse/oniro-core/oniro'
   file:
    - '/.oniro-ci/dco.yaml'
    - '/.oniro-ci/reuse.yaml'
    - '/.oniro-ci/build-generic.yaml'
    - '/.oniro-ci/test-generic.yaml'
    - '/.oniro-ci/machines-and-flavours.yaml'
 - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'

dco:
  extends: .dco
  # https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/issues/499
  allow_failure: true

reuse:
  stage: compliance
  extends: .reuse
  # The repository is not yet reuse compliant and we don't have full control on
  # it - it is a 3rd party fork. Let if fail for now.
  allow_failure: true

# Customize the .workspace job to set the path of the git repository to deviate
# from what the git-repo manifest prepares. This effectively allows testing
# incoming changes that match the repository holding this CI pipeline.
.workspace:
  variables:
    CI_ONIRO_GIT_REPO_PATH: meta-zephyr
# Disable Linux and FreeRTOS builds that are unlikely to be affected by Zephyr
# changes and certainly save some time. Some jobs use .broken template which sets
# when: manual on its own so they are disabled separately below
  rules:
    - if: "$CI_ONIRO_BUILD_FLAVOUR == 'linux'"
      when: never
    - if: "$CI_ONIRO_BUILD_FLAVOUR == 'freertos'"
      when: never
    - when: on_success

## This jobs must be overriden manualy because of .broken rules extention
linux-seco-imx8mm-c61-2gb-clang:
  rules:
    - when: never

linux-seco-imx8mm-c61-4gb-clang:
  rules:
    - when: never

freertos-armv5-clang:
  rules:
    - when: never

linux-seco-imx8mm-c61-4gb-extra-clang:
  rules:
    - when: never

.lava-test-mr:
  extends: .lava-test
  variables:
    CI_LAVA_JOB_PRIORITY: "high"
    CI_SQUAD_GROUP_NAME: "merge-request"
    CI_SQUAD_PROJECT_NAME: "meta-zephyr"

##
## Submit jobs to LAVA
##
lava-zephyr-qemu-cortex-m3:
  needs: [zephyr-qemu-cortex-m3-gcc]
  stage: test
  extends: .lava-test-mr
  variables:
    MACHINE: qemu-cortex-m3
    CI_BUILD_JOB_NAME: zephyr-qemu-cortex-m3-gcc
    CI_LAVA_JOB_DEFINITION: "https://git.ostc-eu.org/OSTC/infrastructure/lava/lava-config/-/raw/master/lava.ostc-eu.org/job-definitions/ci/qemu-zephyr-cortex-m3.yaml"
    CI_REPORT_JOB_NAME: lava-report

lava-zephyr-qemu-x86:
  needs: [zephyr-qemu-x86-gcc]
  stage: test
  extends: .lava-test-mr
  variables:
    MACHINE: qemu-x86
    CI_BUILD_JOB_NAME: zephyr-qemu-x86-gcc
    CI_LAVA_JOB_DEFINITION: "https://git.ostc-eu.org/OSTC/infrastructure/lava/lava-config/-/raw/master/lava.ostc-eu.org/job-definitions/ci/qemu-zephyr-x86.yaml"
    CI_REPORT_JOB_NAME: lava-report

##
## Get the results back from SQUAD.
##
lava-report:
  extends: .lava-report
  dependencies: []
  needs:
    - lava-zephyr-qemu-cortex-m3
    - lava-zephyr-qemu-x86
